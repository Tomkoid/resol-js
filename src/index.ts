import express, { Request, Response } from 'express';
import path from "path";
import winston from "winston";
import expressWinston from "express-winston";

import { Routes, getWebContent } from "./respond"
import { loadEndpoints } from "./api/requests"

import commandLineArgs from "command-line-args";
import { connectRedis, redisClient, setCache } from './cache';
import { startTor } from './tor';

const app = express()
const port = 3009


const transportFile = new winston.transports.File({
  filename: path.join(__dirname + '/../app.log'),
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.uncolorize(),
    winston.format.simple(),
    winston.format.json(),
  ),
})

export const logger = expressWinston.logger({
  transports: [
    new winston.transports.Console(),
    transportFile,
  ],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.cli(),
  )
})

export const infoLogger = winston.createLogger({
  transports: [
    new winston.transports.Console(),
    transportFile,
  ],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.cli(),
  )
})

app.use(logger);

// Page routes
app.get('/', (_, res: any) => { res.send(getWebContent(Routes.Index)) })
app.get('/settings', (_, res: any) => { res.send(getWebContent(Routes.Settings)) })
app.get('/login', (_, res: any) => { res.send(getWebContent(Routes.Login)) })
app.get('/login_qr', (_, res: any) => { res.send(getWebContent(Routes.LoginQR)) })
app.get('/message/:message', (_, res: any) => { res.send(getWebContent(Routes.Message)) })
app.get('/grade/:grade', (_, res: any) => { res.send(getWebContent(Routes.Grade)) })
app.get('/homework/:homework', (_, res: any) => { res.send(getWebContent(Routes.Homework)) })
app.get('/subject/:id/:order', (_, res: any) => { res.send(getWebContent(Routes.Subject)) })
app.get('/dochazka', (_, res: any) => { res.send(getWebContent(Routes.Dochazka)) })
app.get('/hodnoceni', (_, res: any) => { res.send(getWebContent(Routes.Hodnoceni)) })
app.get('/vyuka', (_, res: any) => { res.send(getWebContent(Routes.Vyuka)) })
app.get('/komunikace', (_, res: any) => { res.send(getWebContent(Routes.Komunikace)) })
app.get('/kalendar', (_, res: any) => { res.send(getWebContent(Routes.Kalendar)) })

// Static files
app.use('/styles', express.static(path.join(__dirname, "../web/styles")))
app.use('/scripts', express.static(path.join(__dirname, "../web/scripts")))
app.use('/images', express.static(path.join(__dirname, "../web/images")))
app.use('/manifest.json', express.static(path.join(__dirname, "../web/manifest.json")))
app.use('/sw.js', express.static(path.join(__dirname, "../web/sw.js")))

loadEndpoints(app)

// 404
app.all('*', (_, res: any) => { res.status(404).send(getWebContent(Routes.NotFound)) })

// Option definitions for command line arguments
const optionDefinitions = [
  { name: "redis", alias: "v", type: Boolean, default: false },
  { name: "redisport", type: Number, default: 6379 },
  { name: "redishost", type: String, default: "127.0.0.1" },
  { name: "redisuser", type: String },
  { name: "redispass", type: String },
  { name: "proxycount", alias: "p", type: Number, default: 0 },
]

export const options = commandLineArgs(optionDefinitions)

if (options.redis) {
  connectRedis()
}

app.listen(port, () => {
  setCache("test", { cacheQuery: "hey", expiration: 5 }, { username: "panev", password: "panev123", solUrl: "https://aojff.com"})
  
  if (options.proxycount > 0) {
    infoLogger.info(`Starting another Tor proxies..`)
    startTor(options.proxycount)
  }

  infoLogger.info(`Server successfully started at port ${port}!`)
})
