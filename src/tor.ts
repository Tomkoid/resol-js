import { spawn } from "child_process";
import { SocksProxyAgent } from "socks-proxy-agent";
import { options } from ".";

function getRandomInt(max: number) {
  return Math.floor(Math.random() * max);
}

export function getProxy() {
  if (options.proxycount > 0) {
    const random = 29000 + getRandomInt(options.proxycount);
    return new SocksProxyAgent(`socks://127.0.0.1:${random}`)
  } else {
    return new SocksProxyAgent("socks://127.0.0.1:9050")
  }
}

export async function startTor(amount: number) {
  for (let i = 0; i < amount; i++) {
    startTorService(i)
  }
}

async function startTorService(index: Number) {
  const tor = spawn("tor", ["-f", `tor-group/torrc.${index}`]);
}
