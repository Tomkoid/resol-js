import { Application, Request, Response } from "express"
import { createRequest, getBasicAuth } from "./createRequest"
import { options } from ".."
import { computeSHA256, redisClient, validateRedis } from "../cache"

export enum requestType {
  Login = "solapi/api/connect/token",
  UserInfo = "solapi/api/v1/user",
  Grades = "solapi/api/v1/students", // this is not the full url
  Messages = "solapi/api/v1/messages/received",
  Homework = "solapi/api/v1/students", // this is not the full url
  Attachment = "solapi/api/v1/student/homeworks/assignment/attachments",
  TimeTable = "solapi/api/v1/timeTable",
  Recipients = "solapi/api/v1/messages/recipients"
}

export function loadEndpoints(app: Application) {
  app.get('/api/v1/verify', (req: Request, res: Response) => {
    createRequest(req, res, { requestType: requestType.Login })
  })

  app.get('/api/v1/user', (req: Request, res: Response) => {
    createRequest(req, res, { requestType: requestType.UserInfo, cache_query: "userInfo", cache_expiration: 60000 })
  })

  app.get('/api/v1/grades', (req: Request, res: Response) => {
    createRequest(req, res, { requestType: requestType.Grades, cache_query: "grades" })
  })

  app.get("/api/v1/messages", (req: Request, res: Response) => {
    createRequest(req, res, { requestType: requestType.Messages, cache_query: "messages" })
  })
  
  app.get("/api/v1/homework", (req: Request, res: Response) => {
    createRequest(req, res, { requestType: requestType.Homework, cache_query: "homework" })
  })

  app.get("/api/v1/timetable", (req: Request, res: Response) => {
    createRequest(req, res, { requestType: requestType.TimeTable, cache_query: "timetable" })
  })

  app.get("/api/v1/recipients", (req: Request, res: Response) => {
    createRequest(req, res, { requestType: requestType.Recipients, cache_query: "recipients" })
  })

  app.get("/api/v1/attachment/:attachment", (req: Request, res: Response) => {
    createRequest(req, res, { requestType: requestType.Attachment, cache_query: "attachment" })
  })

  // resol info
  app.get("/api/v1/resol/info", (req: Request, res: Response) => {
    const json = require("../../package.json")

    res.send({
      description: json.description,
      version: json.version,
      homepage: json.repository.url,
      redis: options.redis ? true : false,
      extraTorInstances: options.proxycount,
    })
  })

  // delete cache
  app.get("/api/v1/cache/clean", async (req: Request, res: Response) => {
    if (!validateRedis()) return res.status(400).send({
      error: true,
      specific: "Tato reŠOL instance nepodporuje Redis caching"
    });

    let auth;

    const authBasic = getBasicAuth(req)
    if (authBasic) {
      auth = authBasic
    } else {
      return res.status(400).send({
        error: true,
        message: "No Authorization header"
      })
    }

    if(!req.headers.sol_url) {
      return res.status(400).send({
        error: true,
        message: "No SOL_URL header"
      })
    }

    let authObj: any = {
      username: auth.username,
      password: auth.password,
      solUrl: req.headers.sol_url,
    }
    const username = computeSHA256(authObj.username.toLowerCase())
    const password = computeSHA256(authObj.password)
    const solUrl = computeSHA256(authObj.solUrl)

    let cursor = "0"
    let key: string;

    const reply = await redisClient.scan(cursor, { MATCH: `${username}-${password}-${solUrl}*`, COUNT: 1000 });
    
    if (reply.keys.length == 0) {
      return res.send({
        error: true,
        specific: "Tento uživatel již nemá žádné data v cachi"
      })
    }

    for (key of reply.keys) {
      cursor = reply.cursor;

      await redisClient.del(key);
    }

    res.send({
      error: false
    })
  })
}
