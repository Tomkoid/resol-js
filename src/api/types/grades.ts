import axios, { AxiosError, AxiosResponse } from "axios";
import { getAccessToken, requestAllInformation } from "../createRequest";
import { requestType } from "../requests";
import { getCache, setCache } from "../../cache";

export async function grades(req: requestAllInformation) {
  if (!req.request.query.studentId) {
    return req.response.status(400).send({
      error: true,
      message: "No studentId query was found in the URL query"
    })
  }

  const gradesCache = getCache({
    cacheQuery: `grades-${req.request.query.studentId}`,
  }, req.auth)

  if (await gradesCache) return req.response.send(await gradesCache)
  let token = getAccessToken(req.auth, req.request, req.response);
  if(!token) return

  let gradesQuery = new URLSearchParams({
    "SigningFilter": "all",
    "Pagination.PageNumber": "1",
    "Pagination.PageSize": "30",
  })

  let response: AxiosResponse;
  try {
    response = await axios.request({
      url: `${req.request.headers.sol_url}/${requestType.Grades}/${req.request.query.studentId}/marks/list?${gradesQuery}`,
      method: "GET",
      httpsAgent: req.torProxyAgent,
      httpAgent: req.torProxyAgent,
      headers: {
        Authorization: `Bearer ${await token}`
      }
    })
  } catch(err) {
    return req.response.status(400).send({
      error: true,
      message: "Špatný request",
      specific: (err as AxiosError).message
    })
  }

  if (response.data) {
    setCache(response.data, {
      cacheQuery: `grades-${req.request.query.studentId}`,
      expiration: 60 * 10, // 10 minutes 
    }, req.auth)
    return req.response.send(response.data)
  } else {
    return req.response.status(400).send({
      error: true,
      message: "Žádné data",
    })
  }
}
