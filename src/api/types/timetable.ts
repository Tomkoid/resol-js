import axios, { AxiosError, AxiosResponse } from "axios";
import { getAccessToken, requestAllInformation } from "../createRequest";
import { requestType } from "../requests";
import { getCache, setCache } from "../../cache";

export async function timeTable(req: requestAllInformation) {
  if (!req.request.query.studentId) {
    return req.response.status(400).send({
      error: true,
      message: "No studentId query was found in the URL query"
    })
  }

  const studentId = req.request.query.studentId.toString();

  const timetableCache = getCache({
    cacheQuery: `timetable-${req.request.query.studentId}-${req.request.query.dateFrom!.toString()}-${req.request.query.dateTo!.toString()}`,
  }, req.auth)

  if (await timetableCache) return req.response.send(await timetableCache)
  
  let token = getAccessToken(req.auth, req.request, req.response);
  if(!token) return

  let timeTableQuery = new URLSearchParams({
    "DateFrom": req.request.query.dateFrom!.toString(),
    "DateTo": req.request.query.dateTo!.toString(),
    "StudentId": studentId,
    "SchoolYearId": req.request.query.semesterId!.toString(),
  })

  let response: AxiosResponse;
  try {
    response = await axios.request({
      url: `${req.request.headers.sol_url}/${requestType.TimeTable}?${timeTableQuery}`,
      method: "GET",
      httpsAgent: req.torProxyAgent,
      httpAgent: req.torProxyAgent,
      headers: {
        Authorization: `Bearer ${await token}`
      }
    })
  } catch(err) {
    console.log(err)
    return req.response.status(400).send({
      error: true,
      message: "Špatný request",
      specific: (err as AxiosError).message
    })
  }

  if (response.data) {
    setCache(response.data, {
      cacheQuery: `timetable-${req.request.query.studentId}-${req.request.query.dateFrom!.toString()}-${req.request.query.dateTo!.toString()}`,
      expiration: 60 * 10, // 10 minutes 
    }, req.auth)
    return req.response.send(response.data)
  } else {
    return req.response.status(400).send({
      error: true,
      message: "Žádné data",
    })
  }
}
