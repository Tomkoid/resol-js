import axios, { AxiosError, AxiosHeaders, AxiosResponse } from "axios";
import { getAccessToken, requestAllInformation } from "../createRequest";
import { requestType } from "../requests";
import { getCache } from "../../cache";

export async function attachment(req: requestAllInformation) {
  let token = getAccessToken(req.auth, req.request, req.response);
  if(!token) return

  if (!req.request.query.studentId) {
    return req.response.status(400).send({
      error: true,
      message: "No studentId query was found in the URL query"
    })
  }

  const studentId = req.request.query.studentId.toString();
  const attachmentParams = new URLSearchParams({
    "StudentId": studentId
  })

  let response: AxiosResponse;
  try {
    response = await axios.request({
      url: `${req.request.headers.sol_url}/${requestType.Attachment}/${req.request.params.attachment}?${attachmentParams}`,
      method: "GET",
      httpsAgent: req.torProxyAgent,
      httpAgent: req.torProxyAgent,
      headers: {
        Authorization: `Bearer ${await token}`
      },
      responseType: "arraybuffer",
    })
  } catch(err) {
    return req.response.status(400).send({
      error: true,
      message: "Špatný request",
      specific: (err as AxiosError).message
    })
  }

  if (response.data) {
    if (!response.headers) {
      return req.response.status(400).send({
        error: true,
        message: "Žádné response headery",
      })
    }

    req.response.setHeader("Content-Type", "application/octet-stream");
    req.response.setHeader("Content-Disposition", "attachment; filename=attachment")
    req.response.send(response.data)
  } else {
    return req.response.status(400).send({
      error: true,
      message: "Žádné data",
    })
  }

}
