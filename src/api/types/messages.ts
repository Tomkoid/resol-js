import axios, { AxiosError, AxiosResponse } from "axios";
import { getAccessToken, requestAllInformation } from "../createRequest";
import { requestType } from "../requests";
import { getCache, setCache } from "../../cache";

export async function messages(req: requestAllInformation) {
  const messagesCache = getCache({
    cacheQuery: `messages`,
  }, req.auth)

  if (await messagesCache) return req.response.send(await messagesCache)
  let token = getAccessToken(req.auth, req.request, req.response);
  if(!token) return

  let messagesQuery = new URLSearchParams({
    "Pagination.PageNumber": "1",
    "Pagination.PageSize": "5", 
  })

  let response: AxiosResponse;
  try {
    response = await axios.request({
      url: `${req.request.headers.sol_url}/${requestType.Messages}?${messagesQuery}`,
      method: "GET",
      httpsAgent: req.torProxyAgent,
      httpAgent: req.torProxyAgent,
      headers: {
        Authorization: `Bearer ${await token}`
      }
    })
  } catch(err) {
    return req.response.status(400).send({
      error: true,
      message: "Špatný request",
      specific: (err as AxiosError).message
    })
  }

  if (response.data) {
    setCache(response.data, {
      cacheQuery: `messages`,
      expiration: 60 * 10, // 10 minutes 
    }, req.auth)
    return req.response.send(response.data)
  } else {
    return req.response.status(400).send({
      error: true,
      message: "Žádné data",
    })
  }

}
