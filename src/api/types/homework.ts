import axios, { AxiosError, AxiosResponse } from "axios";
import { getAccessToken, requestAllInformation } from "../createRequest";
import { requestType } from "../requests";
import { getCache, setCache } from "../../cache";

export async function homework(req: requestAllInformation) {
  if (!req.request.query.studentId) {
    return req.response.status(400).send({
      error: true,
      message: "No studentId query was found in the URL query"
    })
  }

  const homeworkCache = getCache({
    cacheQuery: `homework-${req.request.query.studentId}`,
  }, req.auth)

  if (await homeworkCache) return req.response.send(await homeworkCache)
  let token = getAccessToken(req.auth, req.request, req.response);
  if(!token) return


  const studentId = req.request.query.studentId.toString();

  let homeworkQuery = new URLSearchParams({
    "Pagination.PageNumber": "1",
    "Pagination.PageSize": "10",
    "StudentId": studentId,
    "Filter": "active",
  })

  let response: AxiosResponse;
  try {
    response = await axios.request({
      url: `${req.request.headers.sol_url}/${requestType.Homework}/${req.request.query.studentId}/homeworks?${homeworkQuery}`,
      method: "GET",
      httpsAgent: req.torProxyAgent,
      httpAgent: req.torProxyAgent,
      headers: {
        Authorization: `Bearer ${await token}`
      }
    })
  } catch(err) {
    console.log(err)
    return req.response.status(400).send({
      error: true,
      message: "Špatný request",
      specific: (err as AxiosError).message
    })
  }

  if (response.data) {
    setCache(response.data, {
      cacheQuery: `homework-${studentId}`,
      expiration: 60 * 10, // 10 minutes 
    }, req.auth)
    return req.response.send(response.data)
  } else {
    return req.response.status(400).send({
      error: true,
      message: "Žádné data",
    })
  }

}
