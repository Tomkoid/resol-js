import axios, { AxiosError, AxiosResponse } from "axios";
import { getAccessToken, requestAllInformation } from "../createRequest";
import { requestType } from "../requests";
import { getCache, setCache } from "../../cache";

export async function userInfo(req: requestAllInformation) { 
  const userInfoCache = getCache({
    cacheQuery: "user",
  }, req.auth)

  if (await userInfoCache) return req.response.send(await userInfoCache)

  let token = getAccessToken(req.auth, req.request, req.response);

  let response: AxiosResponse;
  try {
    response = await axios.request({
      url: `${req.request.headers.sol_url}/${requestType.UserInfo}`,
      method: "GET",
      httpsAgent: req.torProxyAgent,
      httpAgent: req.torProxyAgent,
      headers: {
        Authorization: `Bearer ${await token}`
      }
    })
  } catch (err) {
    return req.response.status(400).send({
      error: true,
      message: "Špatný request",
      specific: (err as AxiosError).message
    })
  }
  
  if (response.data) {
    setCache(response.data, {
      cacheQuery: "user",
      expiration: 60 * 60 * 24 * 2, // 2 days
    }, req.auth)
    return req.response.send(response.data)
  } else {
    return req.response.status(400).send({
      error: true,
      message: "Žádné data",
    })
  }
}

