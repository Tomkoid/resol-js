import { Request } from "express"
import { requestAllInformation, requestAuthorization } from "../createRequest"
import axios, { AxiosError } from "axios"
import { requestType } from "../requests"

export async function login(req: requestAllInformation) {
  let resp: any;
  
  const params = new URLSearchParams({
    grant_type: "password",
    username: req.auth.username,
    password: req.auth.password,
    client_id: "test_client",
    scope: "openid offline_access profile sol_api"
  })

  try {
    resp = await axios.request({
      url: `${req.request.headers.sol_url}/${requestType.Login}`,
      method: "POST",
      httpsAgent: req.torProxyAgent,
      httpAgent: req.torProxyAgent,
      data: params,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      }
    })
  } catch (err) {
    return req.response.send({ "error": true, "message": "Nepodařilo se přihlásit, prosím zkontrolujte svoje přihlašovací údaje nebo či nemáte zablokovaný účet", specific: (err as AxiosError).message })
  }

  if(resp.data["access_token"]) {
    resp.data["error"] = false
  } else {
    resp.data["error"] = true 
  }

  return req.response.send(resp.data);
}
