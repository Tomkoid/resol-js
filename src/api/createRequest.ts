import axios, { AxiosError } from "axios";
import { Request, Response } from "express";
import { SocksProxyAgent } from "socks-proxy-agent"
import { requestType } from "./requests";
import { URLSearchParams } from "url";
import { getCache, setCache } from "../cache";
import { login } from "./types/login"
import { userInfo } from "./types/user";
import { grades } from "./types/grades";
import { messages } from "./types/messages";
import { homework } from "./types/homework";
import { attachment } from "./types/attachment";
import { timeTable } from "./types/timetable";
import { recipients } from "./types/recipients";
import { getProxy } from "../tor";

export interface requestAuthorization {
  username: string,
  password: string,
  solUrl: string,
  osobaId?: string,
}

export interface requestAllInformation {
  auth: requestAuthorization,
  request: Request,
  response: Response,
  torProxyAgent: SocksProxyAgent,
}

interface requestDetails {
  requestType: requestType,
  tag?: string,
  cache_query?: string,
  cache_expiration?: number
}

export function getBasicAuth(request: Request) {
  if (request.headers.authorization) {
    const b64auth = (request.headers.authorization || '').split(' ')[1] || ''
    const [login, password] = Buffer.from(b64auth, 'base64').toString().split(':')

    return { username: login, password: password };
  } else {
    return null
  }
}


export async function createRequest(request: Request, response: Response, details: requestDetails) {
  const torProxyAgent = getProxy();
  let auth;

  const authBasic = getBasicAuth(request)
  if (authBasic) {
    auth = authBasic
  } else {
    return response.status(400).send({
      error: true,
      message: "No Authorization header"
    })
  }

  if(!request.headers.sol_url) {
    return response.status(400).send({
      error: true,
      message: "No SOL_URL header"
    })
  }

  let token;

  let authObj: any = {
    username: auth.username,
    password: auth.password,
    solUrl: request.headers.sol_url,
  }

  if (request.query.studentId) {
    authObj.studentId = request.query.studentId
  }

  try {
    if (details.requestType == requestType.Login) {
      login({ auth: authObj, request: request, response: response, torProxyAgent: torProxyAgent })
    } else if (details.requestType == requestType.UserInfo) {
      userInfo({ auth: authObj, request: request, response: response, torProxyAgent: torProxyAgent })
    } else if (details.requestType == requestType.Grades && details.cache_query == "grades") {
      grades({ auth: authObj, request: request, response: response, torProxyAgent: torProxyAgent })
    } else if (details.requestType == requestType.Messages) {
      messages({ auth: authObj, request: request, response: response, torProxyAgent: torProxyAgent })
    } else if (details.requestType == requestType.Homework && details.cache_query == "homework") {
      homework({ auth: authObj, request: request, response: response, torProxyAgent: torProxyAgent })
    } else if (details.requestType == requestType.Attachment) {
      attachment({ auth: authObj, request: request, response: response, torProxyAgent: torProxyAgent })
    } else if (details.requestType == requestType.TimeTable) {
      timeTable({ auth: authObj, request: request, response: response, torProxyAgent: torProxyAgent })
    } else if (details.requestType == requestType.Recipients) {
      recipients({ auth: authObj, request: request, response: response, torProxyAgent: torProxyAgent })
    }
  } catch (err) {
    try {
      return response.send({
        error: true,
        message: (err as AxiosError).message
      })
    } catch (_) {
      return
    }
  }

  // if(resp && resp.status == 200 && details.cache_query) {
  //   setCache(resp.data, {
  //     cacheQuery: details.cache_query,
  //     expiration: details.cache_expiration
  //   }, authObj)
  // }

  // return response.send(resp.data)
}

export async function getAccessToken(auth: any, request: Request, response: Response) {
  const torProxyAgent = getProxy();
  const params = new URLSearchParams({
    grant_type: "password",
    username: auth.username,
    password: auth.password,
    client_id: "test_client",
    scope: "openid offline_access profile sol_api"
  })

  let resp;

  try {
    resp = await axios.request({
      url: `${request.headers.sol_url}/${requestType.Login}`,
      method: "POST",
      httpsAgent: torProxyAgent,
      httpAgent: torProxyAgent,
      data: params,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      }
    })
  } catch (err) {
    let message: any = { "error": true, "message": "Nepodarilo se prihlasit", error_message: null }
    if ((err as AxiosError).response!.status == 400) {
      message.error_message = "Spatne uzivatelske jmeno nebo heslo"
    }
    return response.send(message)
  }

  return resp.data.access_token
}
