import { exit } from 'process';
import { createClient } from 'redis';
import { requestAuthorization } from './api/createRequest';
import { options } from './index';
import { createHash } from 'crypto';

export let redisClient: any;

export interface cacheDetails {
  cacheQuery: string,
  expiration?: number, 
}

export async function connectRedis() {
  redisClient = createClient({
    socket: {
      host: options.redishost,
      port: options.redisport,
    },
    username: options.redisuser,
    password: options.redispass
  })

  redisClient.on('error', (err: any) => {
    console.error('Failed to connect to Redis: ', err)
    exit(1);
  });

  await redisClient.connect();
}

export function validateRedis() {
  return options.redis
}

export function computeSHA256(string: string) {
  return createHash("sha256").update(string).digest("hex")
}

export async function setCache(value: string, details: cacheDetails, auth: requestAuthorization) {
  if (!validateRedis()) return;

  if(!details.expiration) {
    details.expiration = 31536000;
  }

  const username = computeSHA256(auth.username.toLowerCase())
  const password = computeSHA256(auth.password)
  const solUrl = computeSHA256(auth.solUrl)
  const cacheQuery = computeSHA256(details.cacheQuery)

  // console.log(`saved to ${username}-${password}-${solUrl}-${cacheQuery} with ${details.expiration} seconds`)

  redisClient.set(
    `${username}-${password}-${solUrl}-${cacheQuery}`,
    JSON.stringify(value),
    { EX: details.expiration }
  )
}

export async function getCache(details: cacheDetails, auth: requestAuthorization) {
  if (!validateRedis()) return null;

  const username = computeSHA256(auth.username.toLowerCase())
  const password = computeSHA256(auth.password)
  const solUrl = computeSHA256(auth.solUrl)
  const cacheQuery = computeSHA256(details.cacheQuery)
 
  const data = redisClient.get(
    `${username}-${password}-${solUrl}-${cacheQuery}`,
  )

  // console.log(`get from ${username}-${password}-${solUrl}-${cacheQuery}`)
  // console.log(await data)

  return (await data ? await data : null)
}
