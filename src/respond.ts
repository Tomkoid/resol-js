import { Request, Response } from "express";
import fs from "fs"

export enum Routes {
  Index = "web/index.html",
  Login = "web/login/login.html",
  LoginQR = "web/login/login_qr.html",
  Message = "web/pages/message.html",
  Grade = "web/pages/grade.html",
  Homework = "web/pages/homework-info.html",
  Subject = "web/pages/subject-info.html",
  Settings = "web/pages/settings.html",
  Dochazka = "web/pages/dochazka/dochazka.html",
  Hodnoceni = "web/pages/hodnoceni/hodnoceni.html",
  Vyuka = "web/pages/vyuka/vyuka.html",
  Komunikace = "web/pages/komunikace/komunikace.html",
  Kalendar = "web/pages/kalendar.html",
  NotFound = "web/404.html",
}

export function getWebContent(route: Routes) {
  try {
    return fs.readFileSync(route, "utf-8")
  } catch (err) {
    console.error("The file that would be returned was not found")
  }
}
