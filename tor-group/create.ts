import { createWriteStream } from "fs"
import { parseArgs } from "node:util";

const {
  values: { amount },
} = parseArgs({
  options: {
    amount: {
      type: "string",
      short: "a",
    },
  },
});


for (let i = 0; i < Number(amount); i++) {
  const stream = createWriteStream(`tor-group/torrc.${i}`)
  stream.once("open", () => {
    stream.write(`SocksPort ${29000 + i}\n`)
    stream.write(`ControlPort ${28000 + i}\n`)
    stream.write(`DataDirectory tor-group-${i}`)
    stream.end()
  })
}
