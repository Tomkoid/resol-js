const convert_date = (date: Date) => {
  const parsed_date = new Date(date)
  
  return parsed_date.toLocaleDateString("cs-cz")
}

const convert_date_time = (date: Date) => {
  const parsed_date = new Date(date)
  
  return `${parsed_date.toLocaleDateString("cs-CZ")} (${parsed_date.toLocaleTimeString("cs-CZ")} ${Intl.DateTimeFormat().resolvedOptions().timeZone})`
}
