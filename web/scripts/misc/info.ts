const get_server_info = async () => {
  const response = await fetch("/api/v1/resol/info", {
    "credentials": "include",
    "method": "GET",
    "mode": "cors"
  });

  const json = await response.json();

  const instanceDescription = document.getElementById("instance-description");
  const instanceVersion = document.getElementById("instance-version");
  const instanceRedis = document.getElementById("instance-uses-redis");
  const instanceTorProxies = document.getElementById("instance-extra-tor-proxies");
  const instanceHomePage = document.getElementById("instance-homepage")!;

  instanceDescription!.textContent = json["description"]
  instanceVersion!.textContent = "Verze serveru: " + json["version"]
  instanceRedis!.textContent = "Používá caching: " + json["redis"]
  instanceTorProxies!.textContent = "Celkový počet Tor proxies: " + (json["extraTorInstances"] + 1) + ` (${json["extraTorInstances"]} extra, 1 master)`
  instanceHomePage.textContent = json["homepage"]
  instanceHomePage.style.color = "var(--surface-1)"
  instanceHomePage.setAttribute("href", json["homepage"])
}
