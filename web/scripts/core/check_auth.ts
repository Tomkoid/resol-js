const check_auth = () => {
  if ((localStorage.getItem("username") == null) || (localStorage.getItem("password") == null) || (localStorage.getItem("sol_url") == null)) {
    window.location.replace("/login");
  }
}

check_auth()
