const logout = () => {
  // User infmation
  localStorage.removeItem("username");
  localStorage.removeItem("password");
  localStorage.removeItem("sol_url");
  localStorage.removeItem("zak");
  localStorage.removeItem("is_rodic");
  localStorage.removeItem("osoba_id");
  localStorage.removeItem("kategorie_id_csv");
  // Caching
  localStorage.removeItem("grades_json_cached");
  localStorage.removeItem("schedule_json_cached");
  localStorage.removeItem("subjects_json_cached");
  // Caching (expire information)
  localStorage.removeItem("grades_refreshed");
  localStorage.removeItem("refreshed_time");
  localStorage.removeItem("subjects_refreshed");
  window.location.replace("/login") // Go to login page
}
