const toggle_schedule = () => {
    const checkbox = document.getElementById("first-checkbox")! as HTMLInputElement
    const main_schedule = document.getElementById("schedule")!
    
    if (checkbox.checked) {
        main_schedule.style.display = "none";
    } else {
        main_schedule.style.display = "block";
    }
}

const toggle_sa_su = () => {
    const checkbox = document.getElementById("second-checkbox")! as HTMLInputElement
    const schedule_sa_row = document.getElementById("schedule-sa-row")!
    const schedule_su_row = document.getElementById("schedule-su-row")!

    if (checkbox.checked) {
        schedule_sa_row.style.display = "none"
        schedule_su_row.style.display = "none"
     
    } else {
        schedule_sa_row.style.display = "grid"
        schedule_su_row.style.display = "grid"
    }
}

toggle_schedule()
toggle_sa_su()
