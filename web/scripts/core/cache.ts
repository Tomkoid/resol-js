const delete_cache = async () => {
  const cacheStatus = document.getElementById("delete-cache-text")!;
  cacheStatus.textContent = "Zjištování statusu..";
  cacheStatus.style.color = "var(--text-color)"

  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")

  // Caching
  localStorage.removeItem("grades_json_cached");
  localStorage.removeItem("schedule_json_cached");
  localStorage.removeItem("subjects_json_cached");

  // Caching (expire information)
  localStorage.removeItem("grades_refreshed");
  localStorage.removeItem("refreshed_time");
  localStorage.removeItem("subjects_refreshed");

  const response = await fetch("/api/v1/cache/clean", {
    "credentials": "include",
    "headers": {
        "Authorization": "Basic " + btoa(user + ":" + pass),
        "SOL_URL": localStorage.getItem("sol_url")!,
    },
    "method": "GET",
    "mode": "cors"
  });

  let json = JSON.parse(await response.text());

  if (json["error"]) {
    cacheStatus.textContent = json["specific"] || json["error"];
    cacheStatus.style.color = "var(--grade-5)"
    return;
  } else {
    cacheStatus.textContent = "🚀 Cache se úspěšně vymazala"
    cacheStatus.style.color = "var(--grade-1)"
  }

  // location.reload() // Reload website
}
