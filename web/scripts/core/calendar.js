let date = new Date();
const daysThisMonth = [];

const renderCalendar = () => {
  date.setDate(1);

  const monthDays = document.querySelector(".days");

  const lastDay = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDate();

  const prevLastDay = new Date(
    date.getFullYear(),
    date.getMonth(),
    0
  ).getDate();

  const firstDayIndex = date.getDay();

  const lastDayIndex = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDay();

  const nextDays = 7 - lastDayIndex - 1;

  const months = [
    "Leden",
    "Únor",
    "Březen",
    "Duben",
    "Květen",
    "Červen",
    "Červenec",
    "Srpen",
    "Září",
    "Říjen",
    "Listopad",
    "Prosinec"
  ];

  var today = new Date();   
  const event = new Date(Date.UTC(new Date().getFullYear(), date.getMonth(), String(today.getDate())));
  const options = {  year: 'numeric', month: 'numeric', day: 'numeric' };
  


  document.querySelector(".date h1").textContent = months[date.getMonth()];
  document.querySelector(".date p").textContent =  event.toLocaleDateString('en-GB', options);

  let days = "";

  for (let x = firstDayIndex; x > 0; x--) {
    days += `<div class="prev-date every-date">${prevLastDay - x + 1}</div>`;
  }

  for (let i = 1; i <= lastDay; i++) {
    if (
      i === new Date().getDate() &&
      date.getMonth() === new Date().getMonth()
    ) {
      days += `<div class="today calendar-date every-date">${i}</div>`;
    } else {
      days += `<div class="calendar-date every-date">${i + " "}</div>`;
      
    }
  }

  for (let j = 1; j <= nextDays; j++) {
    days += `<div class="next-date">${j}</div>`;
    monthDays.innerHTML = days;
  }

  for (let i = 0; i < document.getElementsByClassName("every-date").length; i++) {
    document.getElementsByClassName("prev-date")[i].addEventListener("click", () => {
      daysThisMonth[i] = false
      console.log(document.getElementsByClassName("prev-date")[i].textContent)
      console.log(daysThisMonth[i])
    })

    document.getElementsByClassName("next-date")[i].addEventListener("click", () => {
      daysThisMonth[i] = false
      console.log(document.getElementsByClassName("next-date")[i].textContent)
      console.log(daysThisMonth[i])
    })
    
  }
};



renderCalendar();



//code  - https://github.com/lashaNoz/Calendar
