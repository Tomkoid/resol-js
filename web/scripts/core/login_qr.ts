document.addEventListener("DOMContentLoaded", () => {
  const urlParams = new URLSearchParams(location.search)

  const user = document.getElementById("user")! as HTMLInputElement;
  const pass = document.getElementById("pass")! as HTMLInputElement;
  const solUrl = document.getElementById("sol-url")! as HTMLInputElement;

  for (const [key, value] of urlParams) {
    if(key == "username") {
      user.value = value;
    } else if(key == "password") {
      pass.value = value;
    } else if(key == "sol_url") {
      solUrl.value = value;
    }
  }

  localStorage.removeItem("username");
  localStorage.removeItem("password");
  localStorage.removeItem("sol_url");
  localStorage.removeItem("zak");
  localStorage.removeItem("kategorie_id_csv");
  // Caching
  localStorage.removeItem("grades_json_cached");
  localStorage.removeItem("schedule_json_cached");
  localStorage.removeItem("subjects_json_cached");
  // Caching (expire information)
  localStorage.removeItem("grades_refreshed");
  localStorage.removeItem("refreshed_time");
  localStorage.removeItem("subjects_refreshed");

  verify_auth();
})

