const setTheme = (newTheme: string, save: boolean) => {
    const body = document.getElementsByTagName("body")[0];
    
    body.classList.forEach((token) => {
      body.classList.remove(token);
    });

    body.classList.add(newTheme);

    if (save) {
      localStorage.setItem("mytheme", newTheme);
    }
};

window.onload = () => {
  const themeSwitcher = document.getElementsByName("theme");
  
  for (let i = 0; i < themeSwitcher.length; i++) {
    themeSwitcher[i].addEventListener("change", () => {
      setTheme((document.querySelector('[name="theme"]:checked')! as HTMLInputElement).value, true);
    });
  }
}

let themeOnload = localStorage.getItem("mytheme") || "default";
setTheme(themeOnload, true)
