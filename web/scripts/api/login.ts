// Using development API server
let debug = false;
let debug_server = "http://127.0.0.1:3000";

let limit = false;

const verify_auth = async () => {
    if (limit == true) return;
    limit = true;

    let sol_url = (document.getElementById("sol-url") as HTMLSelectElement).value;
    let user = (document.getElementById("user") as HTMLInputElement).value
    let pass = (document.getElementById("pass") as HTMLInputElement).value

    let customUrl = document.getElementById("custom-url")! as HTMLInputElement;

    if (sol_url == "custom") {
      sol_url = customUrl.value
      if (sol_url.includes("https")) {
        sol_url = customUrl.value
        document.getElementById("pwd-https")!.style.display = "block"
        limit = false
        return
      } else {
        sol_url = "https://" + customUrl.value
      }
    }

    document.getElementById("pwd-failed")!.style.display = "none";
    document.getElementById("pwd-empty")!.style.display = "none";
    document.getElementById("pwd-error")!.style.display = "none";
    document.getElementById("pwd-https")!.style.display = "none"
    document.getElementById("pwd-success")!.style.display = "none";

    if ((user == "" || pass == "" || sol_url == "https://") && sol_url != "https://re-sol.tech") {
      document.getElementById("pwd-empty")!.style.display = "block";
      limit = false;
      return
    }

    if(sol_url == "https://re-sol.tech") {
      user = "1234";
      pass = "1234";
    }
    
    document.getElementById("pwd-loading")!.style.display = "block";
    document.getElementById("login-loader")!.style.display = "block";
 
    let url;
    if (debug) {
      url = debug_server + "/api/v1/verify";
    } else {
      url = "/api/v1/verify"
    }

    var xmlHttp = new XMLHttpRequest();

    xmlHttp.open( "GET", url, true ); // false for synchronous request
    xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
    xmlHttp.setRequestHeader("SOL_URL", sol_url);
    console.log("Sending request..")
    xmlHttp.send();
  
    xmlHttp.onload = () => {
      limit = false;
      document.getElementById("login-loader")!.style.display = "none";
      document.getElementById("pwd-loading")!.style.display = "none"

      if (xmlHttp.responseText.includes("ERROR")) {
        if (xmlHttp.responseText.includes("Proxy server unreachable")) {
          document.getElementById("pwd-proxy")!.style.display = "block";
        } else {
          document.getElementById("pwd-error")!.textContent = xmlHttp.responseText;
          document.getElementById("pwd-error")!.style.display = "block";
        }

        return;
      } else if (xmlHttp.responseText.includes("expected value at line")) {
          document.getElementById("pwd-error")!.textContent = "ERROR: " + xmlHttp.responseText;
          document.getElementById("pwd-error")!.style.display = "block";
          return
      }
      
      const json = JSON.parse(xmlHttp.responseText);

      if (json["error"] == false) {
        localStorage.setItem("username", user);
        localStorage.setItem("password", pass);
        localStorage.setItem("sol_url", sol_url);
        console.log("Successfully authorized!")
        document.getElementById("pwd-success")!.style.display = "block";
        window.location.replace("/");
      } else {
        console.error("Failed to authorize.")
        document.getElementById("pwd-failed")!.textContent = json["message"]
        document.getElementById("pwd-failed")!.style.display = "block";
      }
    };
}
document.getElementById("user")!.addEventListener("keyup", function(event) {
  if(event.key === "Enter") {
    verify_auth()
  }
})

document.getElementById("pass")!.addEventListener("keyup", function(event) {
  if(event.key === "Enter") {
    verify_auth()
  }
})

document.getElementById("custom-url")!.addEventListener("keyup", function(event) {
  if(event.key === "Enter") {
    verify_auth()
  }
})

document.getElementById("enter")!.addEventListener("click", function() {
  verify_auth()
})

if ((document.getElementById("sol-url")! as HTMLSelectElement).value == "custom") {
  (document.getElementById("custom-url") as HTMLInputElement).style.display = "block";
} else if((document.getElementById("sol-url")! as HTMLSelectElement).value == "https://re-sol.tech") {
    document.getElementById('custom-url')!.style.display = 'none';
    document.getElementById('login-username-container')!.style.display = 'none';
    document.getElementById('login-password-container')!.style.display = 'none';
}

const check_auth_login = () => {
  if ((localStorage.getItem("username") != null) && (localStorage.getItem("password") != null) && (localStorage.getItem("sol_url") != null)) {
    window.location.replace("/");
  }
}

document.getElementById("enter-container")!.addEventListener("click", () => {
  console.log("testt")
  document.getElementById("enter-input")!.click();
})
