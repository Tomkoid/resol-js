const get_messages = async () => {
  document.getElementById("dashboard-messages-loader")!.style.display = "block";

  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  let url = "/api/v1/messages"; 

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", url, true ); // false for synchronous request
  xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
  xmlHttp.setRequestHeader("SOL_URL", sol_url);
  xmlHttp.send();

  xmlHttp.onload = () => {
    if (xmlHttp.responseText.includes("ERROR")) {
      document.getElementById("api-error")!.style.display = "flex";
      document.getElementById("api-error")!.textContent = xmlHttp.responseText;
      return;
    }

    localStorage.setItem("messages_json_cached", xmlHttp.responseText);

    let json = JSON.parse(xmlHttp.responseText);

    let messages_count = Object.keys(json["messages"]).length
    for (let i = 0; i < messages_count; i++) {
      if (i > 4){
        break
      }
      document.getElementById("message-" + i + "-date")!.textContent = convert_date(json["messages"][i]["sentDate"].split("T")[0]);
      document.getElementById("message-" + i + "-sender-name")!.innerHTML = "<b>" + json["messages"][i]["sender"]["name"] + "</b>";
      let string = json["messages"][i]["title"];
      if (string.length > 20) {
        string = `${string.slice(0, 26)}...`;
      }
      document.getElementById("message-" + i + "-heading")!.textContent = string;
      document.getElementById("message-" + i + "-heading")!.addEventListener("click", function () {
        window.location.href = "/message/" + json["messages"][i]["id"];
      });
    }
    document.getElementById("dashboard-messages-loader")!.style.display = "none";
  }
}

const message_view = async () => {
  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  let url = "/api/v1/messages"; 

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", url, true ); // false for synchronous request
  xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
  xmlHttp.setRequestHeader("SOL_URL", sol_url);

  let cached = localStorage.getItem("messages_json_cached");

  if(!cached) { 
    document.getElementById("message-loading")!.style.display = "flex";

    xmlHttp.send();

    xmlHttp.onload = () => {
      handle_message_view_data(xmlHttp.responseText);
    }
  } else {
    handle_message_view_data(localStorage.getItem("messages_json_cached")!);
  }
}

const handle_message_view_data = (xmlHttp: string) => {
  const last_segment_url = () => {
    return window.location.pathname.split("/").pop()
  }

  let json = JSON.parse(xmlHttp);

  let messages_count = Object.keys(json["messages"]).length
  for (let i = 0; i < messages_count; i++) {
    if (json["messages"][i]["id"] == last_segment_url()) {
      document.getElementById("message-title")!.textContent = json["messages"][i]["title"];
      document.getElementById("message-author")!.textContent = json["messages"][i]["sender"]["name"];
      document.getElementById("message-content")!.innerHTML = json["messages"][i]["text"];
      document.getElementById("message-loading")!.style.display = "none";
      document.getElementById("message-container")!.style.display = "flex";

      if (json["messages"][i]["text"].includes("Jedná se o zprávu s přílohou. Použijte webovou aplikaci, kde si můžete přílohu zobrazit.")) document.getElementById("message-attachment-error")!.style.visibility = "visible"; 

      return
    }
  }
  console.error("not found")
}

get_messages()
