const get_homework = async () => {
  document.getElementById("dashboard-homework-loader")!.style.display = "flex"
  
  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  await checkStudentId();
  const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;

  let url = `/api/v1/homework?studentId=${studentId}`;

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", url, true ); // false for synchronous request
  xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
  xmlHttp.setRequestHeader("SOL_URL", sol_url);
  xmlHttp.send();

  xmlHttp.onload = () => {
    document.getElementById("dashboard-homework-loader")!.style.display = "none"
    if (xmlHttp.responseText.includes("ERROR")) {
      document.getElementById("api-error")!.style.display = "flex";
      document.getElementById("api-error")!.textContent = xmlHttp.responseText;
      return
    }

    handle_homework_data(xmlHttp.responseText)
  }
}

const homework_view = () => {
  
  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  let url = "/api/homework";

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", url, true ); // false for synchronous request
  xmlHttp.setRequestHeader("Username", user!);
  xmlHttp.setRequestHeader("Password", pass!);
  xmlHttp.setRequestHeader("SOL_URL", sol_url);
  xmlHttp.setRequestHeader("OSOBA_ID", localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!);

  let cached = localStorage.getItem("homework_json_cached");

  if(!cached) {
    document.getElementById("homework-loading")!.style.display = "flex"
    xmlHttp.send();

    xmlHttp.onload = () => {
      document.getElementById("homework-loading")!.style.display = "none"
      if (xmlHttp.responseText.includes("ERROR")) {
        document.getElementById("api-error")!.style.display = "flex";
        document.getElementById("api-error")!.textContent = xmlHttp.responseText;
        return
      }

      handle_homework_view_data(xmlHttp.responseText)
    } 
  } else {
    handle_homework_view_data(cached)
  }
}

const handle_homework_data = (xmlHttp: string) => {
  localStorage.setItem("homework_json_cached", xmlHttp);

  let json = JSON.parse(xmlHttp);
  let homework_count = Object.keys(json["homeworks"]).length

  for (let i = 0; i < homework_count; i++) {
    if (i > 9) {
      break
    }

    document.getElementById("homework-box-cell-date-" + i)!.textContent = convert_date(json["homeworks"][i]["dateEnd"].split("T")[0]);
    document.getElementById("homework-box-cell-sender-name-" + i)!.innerHTML = "<b>" + json["homeworks"][i]["teacher"]["displayName"] + "</b>";

    let nazev_ukolu = json["homeworks"][i]["name"];
    if (nazev_ukolu.length > 23) {
      nazev_ukolu = `${nazev_ukolu.slice(0, 26)}...`;
    }

    document.getElementById("homework-box-cell-heading-" + i)!.textContent = nazev_ukolu;

    let ukolId = json["homeworks"][i]["id"];
    document.getElementById("homework-box-cell-heading-" + i)!.addEventListener("click", () => {
      window.location.href = "/homework/" + ukolId;
    })
  }
}

const handle_homework_view_data = async (xmlHttp: string) => {
  const last_segment_url = () => {
    return window.location.pathname.split("/").pop()
  }

  await checkStudentId();
  const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;

  let json = JSON.parse(xmlHttp);
  console.log(json)
  let homework_count = Object.keys(json["homeworks"]).length

  for (let i = 0; i < homework_count; i++) {
    if (last_segment_url() == json["homeworks"][i]["id"]) {
      document.getElementById("homework-container")!.style.display = "flex";
      document.getElementById("homework-title")!.textContent = json["homeworks"][i]["name"];
      document.getElementById("homework-subject")!.innerHTML = "<b>" + json["homeworks"][i]["subject"]["name"] + "</b>";
      document.getElementById("homework-content")!.innerHTML = json["homeworks"][i]["content"];

      const attachmentsLength = Object.keys(json["homeworks"][i]["attachments"]).length;
      for (let j = 0; j < attachmentsLength; j++) {
        const attachment = document.createElement("div")
        attachment.className = "attachment"

        const attachmentName = document.createElement("p")
        attachmentName.textContent = json["homeworks"][i]["attachments"][j]["name"]

        const attachmentDownload = document.createElement("button")
        attachmentDownload.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path fill="currentColor" d="M6 20q-.825 0-1.413-.588T4 18v-3h2v3h12v-3h2v3q0 .825-.588 1.413T18 20H6Zm6-4l-5-5l1.4-1.45l2.6 2.6V4h2v8.15l2.6-2.6L17 11l-5 5Z"/></svg>`

        const attachmentId = json["homeworks"][i]["attachments"][j]["id"];
        attachmentDownload.addEventListener("click", async () => {
          const downloadAlert = document.createElement("p")
          downloadAlert.textContent = "Stahování může chvíli trvat, prosím počkejte."
          downloadAlert.style.marginTop = "20px";
          downloadAlert.style.color = "var(--primary-text)";

          document.getElementById("attachments")!.appendChild(downloadAlert!);

          await fetch(`/api/v1/attachment/${attachmentId}?studentId=${studentId}`, {
            "method": "GET",
            "headers": {
              "Authorization": `Bearer ${btoa(`${localStorage.getItem("username")}:${localStorage.getItem("password")}`)}`,
              "SOL_URL": localStorage.getItem("sol_url")!,
            },
          }).then(res => res.blob())
            .then(data => {
              var a = document.createElement("a");
              a.href = window.URL.createObjectURL(data);
              a.download = json["homeworks"][i]["attachments"][j]["name"];
              a.click(); 
            });

            downloadAlert.style.display = "none";
        })

        attachment.appendChild(attachmentDownload)
        attachment.appendChild(attachmentName)
        document.getElementById("attachments")!.appendChild(attachment)
      }
    }
  }
}
