const last_segment_url = () => {
  return window.location.pathname.split("/").pop()
}

const get_event_id = () => {
  return window.location.pathname.split("/")[2]
}

const get_student_id = () => {
  return window.location.pathname.split("/")[3]
}

const get_grades = async () => {
  document.getElementById("dashboard-grades-loader")!.style.display = "";
  await checkStudentId();

  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;
  let url = `/api/v1/grades?studentId=${studentId}`;

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", url, true ); // false for synchronous request
  xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
  xmlHttp.setRequestHeader("SOL_URL", sol_url);

  // If subjects json or grades json not cached
  xmlHttp.send();
  xmlHttp.onload = () => {
    handle_grades_data(xmlHttp.responseText)
  }
}

const handle_grades_data = (mainResponse: string) => {
  if (mainResponse.includes("ERROR")) {
    document.getElementById("api-error")!.style.display = "flex";
    document.getElementById("api-error")!.textContent = "error occurred when fetching grades or subjects";
    return;
  }

  localStorage.setItem("grades_json_cached", mainResponse);

  let json = JSON.parse(mainResponse);

  let znamky = [];

  let grades_count = Object.keys(json["marks"]).length
  let j = 0;
  for (let i = 0; i < grades_count; i++) {
    console.log(`index: ${i}, j: ${j}, mark: ${json["marks"][j]["markText"]}`)
    if (i > 9){
      break
    }

    // if mark is not received yet
    if(json["marks"][j]["markText"] == "-") {
      j++;
      i--;
      continue;
    }

    document.getElementById("grade-" + i + "-date")!.textContent = json["marks"][j]["markDate"].split("T")[0];
    document.getElementById("grade-" + i + "-grade")!.textContent = json["marks"][j]["markText"];
    document.getElementById("grade-" + i + "-grade")!.style.color = assign_grade_color(Number(json["marks"][j]["markText"]));
    znamky.push(Number(json["marks"][j]["markText"]));

    document.getElementById("grade-" + i + "-grade")!.style.cursor = "pointer";
      
    let gradeId = json["marks"][j]["id"];
    document.getElementById("grade-" + i + "-grade")!.addEventListener("click", () => {
      window.location.href = "/grade/" + gradeId;
    })

    let subjects_count = Object.keys(json["subjects"]).length
    
    for (let x = 0; x < subjects_count; x++) {
      if(json["subjects"][x]["id"] == json["marks"][j]["subjectId"]) {
        document.getElementById("grade-" + i + "-name")!.textContent = json["subjects"][x]["abbrev"];
        break;
      }
    }

    j++
  }

  create_grade_average(znamky);

  document.getElementById("dashboard-grades-loader")!.style.display = "none";
}

const grade_view = async () => {
  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  await checkStudentId()
  const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;
  let url = `/api/v1/grades?studentId=${studentId}`;

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", url, true ); // false for synchronous request
  xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
  xmlHttp.setRequestHeader("SOL_URL", sol_url);

  document.getElementById("loading-page-info-data")!.style.display = "flex"; // Show loader

  xmlHttp.send();

  xmlHttp.onload = () => {
    handle_grades_view_data(xmlHttp.responseText)
  }
}

const handle_grades_view_data = (xmlHttp: string) => {
  let json = JSON.parse(xmlHttp);

  console.log(json);

  const marksLength = Object.keys(json["marks"]).length;
  for (let i = 0; i < marksLength; i++) {
    if (json["marks"][i]["id"] == last_segment_url()) {
      // teacher and subject is not known in the marks section of the json
      let teacher: string = "Neznámý/á";
      let subject: string = "Neznámý";

      const teacherLength = Object.keys(json["teachers"]).length;
      const subjectLength = Object.keys(json["subjects"]).length;

      // get teacher
      for (let j = 0; j < teacherLength; j++) {
        if (json["marks"][i]["teacherId"] == json["teachers"][j]["id"]) {
          teacher = json["teachers"][j]["displayName"]
        }
      }

      // get subject
      for (let j = 0; j < subjectLength ; j++) {
        if (json["marks"][i]["subjectId"] == json["subjects"][j]["id"]) {
          subject = json["subjects"][j]["name"]
        }
      }
      // document.getElementById("grade-year")!.textContent = json["Data"]["SKOLNI_ROK_NAZEV"];
      // document.getElementById("grade-period")!.textContent = json["Data"]["POLOLETI_NAZEV"];
      document.getElementById("grade-hour")!.textContent = json["marks"][i]["lessonId"];
      document.getElementById("grade-subject")!.textContent = subject;
      document.getElementById("grade-teacher")!.textContent = teacher;
      document.getElementById("grade-date")!.textContent = convert_date(json["marks"][i]["markDate"]);
      document.getElementById("grade-title")!.textContent = json["marks"][i]["theme"];
      document.getElementById("grade-result")!.textContent = json["marks"][i]["markText"];
      document.getElementById("grade-result")!.style.color = assign_grade_color(Number(json["marks"][i]["markText"]));
      document.getElementById("grade-average")!.textContent = json["marks"][i]["classAverage"];
      document.getElementById("grade-rank")!.textContent = json["marks"][i]["classRankText"];
      document.getElementById("grade-weight")!.textContent = json["marks"][i]["weight"];
      if (json["marks"][i]["comment"]) document.getElementById("grade-note")!.textContent = json["marks"][i]["comment"];
      document.getElementById("grade-rating")!.textContent = json["marks"][i]["typeNote"];
      document.getElementById("page-info-container")!.style.display = "flex";

      document.getElementById("loading-page-info-data")!.style.display = "none"; // Hide loader
      return
    }
  }
}

const create_grade_average = (znamky: number[]) => {
  const average = (arr: number[]) => arr.reduce( ( p, c ) => p + c, 0 ) / arr.length;
  
  let result = average(znamky)
  
  document.getElementById("prumer-znamek")!.textContent = (result as unknown) as string;
  document.getElementById("prumer-znamek")!.style.color = assign_grade_color(result)!;
}

const assign_grade_color = (grade: number) => {
  let grade_1_color = window.getComputedStyle(document.body).getPropertyValue('--grade-1');
  let grade_2_color = window.getComputedStyle(document.body).getPropertyValue('--grade-2');
  let grade_3_color = window.getComputedStyle(document.body).getPropertyValue('--grade-3');
  let grade_4_color = window.getComputedStyle(document.body).getPropertyValue('--grade-4');
  let grade_5_color = window.getComputedStyle(document.body).getPropertyValue('--grade-5');
  
  if(Math.round(grade) == 1) {
    return grade_1_color
  } else if(Math.round(grade) == 2) {
    return grade_2_color
  } else if(Math.round(grade) == 3) {
    return grade_3_color
  } else if(Math.round(grade) == 4) {
    return grade_4_color
  } else {
    return grade_5_color
  }
}
