const get_monday = (d: Date) => {
  d = new Date(d);
  var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}

const get_day_of_the_week = (day: number) => {
  switch (day) {
    case 1:
      return "po"
    case 2:
      return "ut"
    case 3:
      return "st"
    case 4:
      return "ct"
    case 5:
      return "pa"
  }
}

const load_schedule = async () => {
  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");
  
  await checkStudentId();
  const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;
  let date = get_monday(new Date())

  let month: any = (date.getMonth() + 1) as number;
  
  if(date.getMonth().toString().length < 2) {
    month = "0" + month;
  }
  
  const monday = date.getFullYear() + "-" + month + "-" + date.getDate()
    
  date.setDate(date.getDate() + 4)
  
  month = date.getMonth() + 1

  if(date.getMonth().toString().length < 2) {
    month = "0" + month;
  }
  
  const friday = date.getFullYear() + "-" + month + "-" + date.getDate()

  const params = new URLSearchParams({
    "studentId": studentId,
    "dateFrom": monday,
    "dateTo": friday,
    "semesterId": localStorage.getItem("semester_id")!
  })

  let url = `/api/v1/timetable?${params}`; 
  // if (debug) {
  //   url = debug_server + "/api/verify";
  // } else {
  //   url = "/api/verify"
  // }
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", url, true ); // false for synchronous request
  xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
  xmlHttp.setRequestHeader("SOL_URL", sol_url);
  xmlHttp.send();

  xmlHttp.onload = () => {
    if (xmlHttp.responseText.includes("ERROR")) {
      document.getElementById("api-error")!.style.display = "flex";
      document.getElementById("api-error")!.textContent = xmlHttp.responseText;
      return;
    }

    localStorage.setItem("schedule_json_cached", xmlHttp.responseText);
    let json = JSON.parse(xmlHttp.responseText);

    for (let i = 0; i < Object.keys(json["lessons"]).length; i++) {
      const currentLesson = json["lessons"][i];
      const beginTime = currentLesson["beginTime"].split("T")[1].split(":");
      const endTime = currentLesson["endTime"].split("T")[1].split(":");

      if (Number(currentLesson["id"]) > 9) {
        continue;
      }

      document.getElementById(`subject-hour-length-${currentLesson["id"]}`)!.textContent = `${beginTime[0]}:${beginTime[1]}-${endTime[0]}:${endTime[1]}`
    }

    // Count JSON objects
    const daysCount = Object.keys(json["days"]).length
    for (let j = 0; j < daysCount; j++) {
      const schedulesCount = Object.keys(json["days"][j]["schedules"]).length

      for (let i = 0; i < schedulesCount; i++) {
        const currentSchedule = json["days"][j]["schedules"][i];

        // If subject is not null
        if (currentSchedule["subject"]["id"] != null) {
          let zkratka = currentSchedule["subject"]["abbrev"] // Subject name in short
          let class_name = currentSchedule["rooms"][0]["abbrev"] // Get class

          // Time
          let hour = currentSchedule["lessonIdFrom"];
          let day_of_the_week = new Date(currentSchedule["beginTime"]).getDay()
          let day_name = get_day_of_the_week(day_of_the_week);

          // if hour is not present, skip
          // i don't get why they don't include some boolean in the api something like: odpadnuto: true
          if(currentSchedule["hourColour"] == "238,238,238") {
            continue
          }

          document.getElementById("subject-" + day_name + "-" + hour)!.innerHTML = "<p>" + zkratka + "</p><p class='subject-class'>" + class_name + "</p>"; // Replace textContent as subject name in short

          document.getElementById("subject-" + day_name + "-" + hour)!.style.backgroundColor = "var(--surface-1)"
          document.getElementById("subject-" + day_name + "-" + hour)!.style.border = "3px solid var(--surface-0)"
          document.getElementById("subject-" + day_name + "-" + hour)!.style.cursor = "pointer";

          // Add click event listener
          document.getElementById("subject-" + day_name + "-" + hour)!.addEventListener("click", function () {
            let event_id = currentSchedule["scheduledHourId"] // Get event ID
            let poradi = currentSchedule["scheduledHourOrder"] // Get subject date 
            window.location.href = "/subject/" + event_id + "/" + poradi
          })
          if (currentSchedule["hourType"] != null) {
            const typ_udalosti_id = currentSchedule["hourType"]["id"] // Get event type
            // If hour is substituted, set different backgroundColor
            if (typ_udalosti_id == "SUPLOVANI") {
              document.getElementById("subject-" + day_name + "-" + hour)!.style.backgroundColor = getComputedStyle(document.body).getPropertyValue("--substituted-hour"); 
            }
          }
        } else {
          // If event is not a regular schedule hour
          if(currentSchedule["hourType"]["id"] == "SKOLNI_AKCE") {
            // Create new box inside of events dashboard
            const box_row_element = document.createElement("div");
            box_row_element.className = "box-row";

            const box_date = document.createElement("p");
            box_date.className = "box-date";
            box_date.textContent = convert_date(currentSchedule["beginTime"])

            const event_description = document.createElement("p");
            event_description.className = "event-description";
            event_description.textContent = currentSchedule["title"];

            document.getElementById("dashboard-events-container")!.appendChild(box_row_element);
            box_row_element.appendChild(box_date)
            box_row_element.appendChild(event_description)

            let hour = currentSchedule["lessonIdFrom"];
            let day_of_the_week = new Date(currentSchedule["beginTime"]).getDay()
            let day_name = get_day_of_the_week(day_of_the_week);

            // Add click event listener
            document.getElementById("subject-" + day_name + "-" + hour)!.addEventListener("click", function () {
              let event_id = currentSchedule["scheduledHourId"] // Get event ID
              let poradi = currentSchedule["scheduledHourOrder"] // Get subject date 
              window.location.href = "/subject/" + event_id + "/" + poradi
            })

            document.getElementById("subject-" + day_name + "-" + hour)!.style.backgroundColor = "var(--grade-3)"
            document.getElementById("subject-" + day_name + "-" + hour)!.style.color= "var(--primary-background)"
            document.getElementById("subject-" + day_name + "-" + hour)!.style.cursor = "pointer";
            document.getElementById("subject-" + day_name + "-" + hour)!.innerHTML = `<p>${currentSchedule["hourType"]["displayName"]}</p><p class="subject-class">${currentSchedule["groups"][0]["className"]}</p>`;
          } else {
            let hour = currentSchedule["lessonIdFrom"];
            let day_of_the_week = new Date(currentSchedule["beginTime"]).getDay()
            let day_name = get_day_of_the_week(day_of_the_week);

            document.getElementById("subject-" + day_name + "-" + hour)!.textContent = "X";
            document.getElementById("subject-" + day_name + "-" + hour)!.style.color = "red";
            document.getElementById("subject-" + day_name + "-" + hour)!.style.fontWeight = "bold";
            console.error("Cannot interpret event at day " + day_of_the_week + ", hour " + hour + ".")
          }
        }
      }
    }
    (document.getElementsByClassName("loading-schedule-data")[0] as HTMLElement)!.style.display = "none"; // Disable schedule data loader
  };
}

load_schedule()
