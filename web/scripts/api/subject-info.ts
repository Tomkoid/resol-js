const get_poradi = () => {
  return window.location.pathname.split("/")[3]
}

const load_subject_info = async () => {
  const get_event_id = () => {
    return window.location.pathname.split("/")[2]
  }
  await checkStudentId();
  const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;
  let date = get_monday(new Date())

  let month: any = (date.getMonth() + 1) as number;
  
  if(date.getMonth().toString().length < 2) {
    month = "0" + month;
  }
  
  const monday = date.getFullYear() + "-" + month + "-" + date.getDate()
    
  date.setDate(date.getDate() + 4)
  
  month = date.getMonth() + 1

  if(date.getMonth().toString().length < 2) {
    month = "0" + month;
  }
  
  const friday = date.getFullYear() + "-" + month + "-" + date.getDate()

  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  const params = new URLSearchParams({
    "studentId": studentId,
    "dateFrom": monday,
    "dateTo": friday,
    "semesterId": localStorage.getItem("semester_id")!
  })

  let url = `/api/v1/timetable?${params}`; 
  // if (debug) {
  //   url = debug_server + "/api/verify";
  // } else {
  //   url = "/api/verify"
  // }
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", url, true ); // false for synchronous request
  xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
  xmlHttp.setRequestHeader("SOL_URL", sol_url);

  document.getElementById("loading-page-info-data")!.style.display = "flex";
  xmlHttp.send();
  xmlHttp.onload = () => {
    handle_subject_info_data(xmlHttp.responseText)
  };
}

function handle_subject_info_data(xmlHttp: string) {
  const get_day_of_the_week = (day: number) => {
    switch (day) {
      case 1:
        return "po"
      case 2:
        return "ut"
      case 3:
        return "st"
      case 4:
        return "ct"
      case 5:
        return "pa"
    }
  }

  let json = JSON.parse(xmlHttp);
  console.log(json);

  // console.log(event_id + "/" + get_event_id())
  // console.log(subject_date + "/" + get_date_subject())

  const daysCount = Object.keys(json["days"]).length

  for (let j = 0; j < daysCount; j++) {
    const schedulesCount = Object.keys(json["days"][j]["schedules"]).length

    for (let i = 0; i < schedulesCount; i++) {
      const currentSchedule = json["days"][j]["schedules"][i];

      let event_id = currentSchedule["scheduledHourId"] 
      let poradi = currentSchedule["scheduledHourOrder"]    

      // If subject is not null
      if (event_id == get_event_id() && poradi == get_poradi() && currentSchedule["subject"]["id"] != null) {
        let subject_name = currentSchedule["subject"]["name"] // Subject subject_name
        let subject_date = convert_date(currentSchedule["beginTime"].replaceAll("T00:00:00", ""));
        
        let class_name = currentSchedule["groups"][0]["className"] // Get class
        let subject_teacher_name = currentSchedule["teachers"][0]["displayName"]
        // theme is not here because it needs to make another request just to get the hour theme
      
        document.getElementById("subject-name")!.textContent = subject_name;
        document.getElementById("subject-date")!.textContent = subject_date;
        document.getElementById("subject-class-name")!.textContent = class_name;
        document.getElementById("subject-teacher")!.textContent = subject_teacher_name;
      }
      (document.getElementsByClassName("loading-page-info-data")[0] as HTMLElement).style.display = "none"; // Disable schedule data loader
      (document.getElementsByClassName("page-info-container")[0] as HTMLElement).style.display = "flex"; // Show subject info container
    }
  }
}


load_subject_info()
