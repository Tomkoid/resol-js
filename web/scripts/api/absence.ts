const get_absence = async () => {
    document.getElementById("dashboard-absence-loader")!.style.display = "flex";

    let user = localStorage.getItem("username")
    let pass = localStorage.getItem("password")
    let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");
  
    let url = "/api/absence";

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", url, true ); // false for synchronous request
    xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
    xmlHttp.setRequestHeader("SOL_URL", sol_url);
    if(localStorage.getItem("selected_zak") || localStorage.getItem("osoba_id")) xmlHttp.setRequestHeader("OSOBA_ID", localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!);
    xmlHttp.send();

    xmlHttp.onload = () => {
        document.getElementById("dashboard-absence-loader")!.style.display = "none"
        if (xmlHttp.responseText.includes("ERROR")) {
          document.getElementById("api-error")!.style.display = "flex";
          document.getElementById("api-error")!.textContent = xmlHttp.responseText;
          return
        }

        let json = JSON.parse(xmlHttp.responseText);

        let date_today = new Date().toISOString().slice(0, 10);
        let absence_count = Object.keys(json["Data"]).length;

        for (let i = 0; i < absence_count; i++){
            if (date_today == json["Data"][i]["DATUM"].split("T")[0]) {
                document.getElementById("box-row-celkem")!.style.display = "flex";
                document.getElementById("today-missed")!.textContent = json["Data"][i]["ABSENCE_CELKEM"];
                
                if (json["Data"][i]["NEVYHODNOCENO"] != "0") {
                    document.getElementById("box-row-nevyhodnoceno")!.style.display = "flex";
                    document.getElementById("today-missed-nevyhodnoceno")!.textContent = json["Data"][i]["NEVYHODNOCENO"];
                }
                if (json["Data"][i]["OMLUVENO"] != "0") {
                    document.getElementById("box-row-omluveno")!.style.display = "flex";
                    document.getElementById("today-missed-omluveno")!.textContent = json["Data"][i]["OMLUVENO"];
                }
                if (json["Data"][i]["NEOMLUVENO"] != "0") {
                    document.getElementById("box-row-neomluveno")!.style.display = "flex";
                    document.getElementById("today-missed-neomluveno")!.textContent = json["Data"][i]["NEOMLUVENO"];
                }
                if (json["Data"][i]["NEZAPOCITANO"] != "0") {
                    document.getElementById("box-row-nezapocitano")!.style.display = "flex";
                    document.getElementById("today-missed-nezapocitano")!.textContent = json["Data"][i]["NEZAPOCITANO"];
                }
            } else {
                document.getElementById("box-row-celkem")!.style.display = "flex";
            }
        }
    }
}
