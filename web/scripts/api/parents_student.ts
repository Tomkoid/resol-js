let isDone = false;

const get_parents = async () => {
    // document.getElementById("dashboard-absence-loader").style.display = "flex";

    let user = localStorage.getItem("username")
    let pass = localStorage.getItem("password")
    let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");
  
    let url = "/api/parents_student";

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", url, true ); // false for synchronous request
    xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
    xmlHttp.setRequestHeader("SOL_URL", sol_url);
    xmlHttp.send();

    xmlHttp.onload = () => {
        // document.getElementById("dashboard-absence-loader").style.display = "none"
        if (xmlHttp.responseText.includes("ERROR")) {
          document.getElementById("api-error")!.style.display = "flex";
          document.getElementById("api-error")!.textContent = xmlHttp.responseText;
          return
        }

        let json = JSON.parse(xmlHttp.responseText);

        let zak_count = Object.keys(json["Data"]).length;

        for(let i = 0; i < zak_count; i++) {
          let zak = document.createElement("option");
          zak.setAttribute("value", json["Data"][i]["OSOBA_ID"])
          zak.textContent = json["Data"][i]["JMENO"];

          document.getElementById("zak-chooser-select")!.appendChild(zak)

          if (!localStorage.getItem("selected_zak")) {
            localStorage.setItem("selected_zak", json["Data"][i]["OSOBA_ID"]);
            break;
          }

        }

        if (localStorage.getItem("selected_zak")) {
          (document.getElementById("zak-chooser-select")! as HTMLSelectElement).value = localStorage.getItem("selected_zak")!;
        }


        document.getElementById("zak-chooser")!.style.display = "flex";
        isDone = true;
        return;
    }
}

const select_zak = (value: string) => {
  localStorage.setItem("selected_zak", value);

  window.location.reload();
}

window.addEventListener("load", async () => {
  if (localStorage.getItem("is_rodic") == "true" && localStorage.getItem("selected_zak") == null) {
    await get_parents();
    setInterval(() => {
      if(isDone) {
        window.location.reload();
      }
    }, 300)
  }
})
