const load_user_info = async () => {
    const expiration_duration = 1000 * 60 * 60 * 24; // 24 hours
    const current_time = new Date().getTime()
    
    const expired = current_time - ((localStorage.getItem("refreshed_time") as unknown) as number) > expiration_duration;
    if (expired) {
      localStorage.removeItem("zak")
      localStorage.removeItem("kategorie_id_csv")
    }
    if ((localStorage.getItem("zak") != null) && (localStorage.getItem("kategorie_id_csv") != null)) {
      document.getElementById("zak")!.textContent = localStorage.getItem("zak");
      document.getElementById("zak-header")!.innerHTML = localStorage.getItem("zak") + " <br> " + localStorage.getItem("kategorie_id_csv");
      (document.getElementsByClassName("loading-data")[0] as HTMLElement).style.display = "none";
      return
    }
    let user = localStorage.getItem("username")!
    let pass = localStorage.getItem("password")
    let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");
    let url = "/api/v1/user"; 

    var xmlHttp = new XMLHttpRequest();

    xmlHttp.open( "GET", url, true ); // false for synchronous request
    xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
    xmlHttp.setRequestHeader("USER_ID", user)
    xmlHttp.setRequestHeader("SOL_URL", sol_url);
    xmlHttp.send();
  
    xmlHttp.onload = () => {
      const json = JSON.parse(xmlHttp.responseText);
      if (json["error"]) {
        document.getElementById("api-error")!.style.display = "flex";
        document.getElementById("api-error")!.textContent = `UserInfo: ${json["specific"] || json["error"]}`;
        return
      }

      document.getElementById("zak")!.textContent = json["fullName"];
      
      localStorage.setItem("zak", json["fullName"]);
      
      document.getElementById("zak-header")!.innerHTML = `${json["fullName"]} <br> ${json["userTypeText"]}`;
     
      let is_rodic = json["userTypeText"].includes("KAT_RODIC")

      localStorage.setItem("kategorie_id_csv", json["userTypeText"])
      localStorage.setItem("is_rodic", is_rodic);
      localStorage.setItem("osoba_id", json["personID"]);
      localStorage.setItem("semester_id", json["schoolYearId"]);
      
      const currentTime = new Date().getTime();

      localStorage.setItem("refreshed_time", (currentTime as unknown) as string);

      (document.getElementsByClassName("loading-data")[0] as HTMLElement).style.display = "none";

      if(is_rodic) {
        get_parents();
        window.location.reload();
      }
    };
}

async function get_main_teacher() {
    let user = localStorage.getItem("username")
    let pass = localStorage.getItem("password")
    let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");
    let url = "/api/v1/recipients"; 
    // if (debug) {
    //   url = debug_server + "/api/verify";
    // } else {
    //   url = "/api/verify"
    // }

    var xmlHttp = new XMLHttpRequest();

    xmlHttp.open( "GET", url, true ); // false for synchronous request
    xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
    xmlHttp.setRequestHeader("SOL_URL", sol_url)
    xmlHttp.send();
  
    xmlHttp.onload = () => {
      let json = JSON.parse(xmlHttp.responseText);

      for(let i = 0; i < Object.keys(json["recipients"]).length; i++) {
        if(json["recipients"][i]["detailName"] == "třídní učitel") {
          document.getElementById("tridni")!.textContent = `Třídní učitel/ka: ${json["recipients"][i]["displayName"]}`

          return
        }
      }
    };
}

load_user_info()
get_main_teacher()
