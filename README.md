![banner](assets/resol-banner.jpg)

<h1 align="center">reŠOL</h3>

<p align="center">
    Svobodná alternativní aplikace pro Školu Online
    <br />
    <a href="https://codeberg.org/resol/resol/wiki"><strong>Prohlédnout dokumentaci »</strong></a>
    <br />
    <br />
    <a href="https://re-sol.tech">Oficiální instance</a>
    ·
    <a href="https://codeberg.org/resol/resol/issues">Nahlásit chybu</a>
    ·
    <a href="https://codeberg.org/resol/resol/issues">Požádat o funkci</a>
</p>

<h1>O projektu</h1>

Tento projekt jsme začali kvůli neustálé nespokojenosti se Školou OnLine. Jejich web je zastaralý, neresponzivní a nepraktický. Mnoho věcí je řešeno neintuitivně, například průměry známek na některých školách. Nová aplikace sice napravila problém se vzhledem, ale stále jí chybí funkce jako tmavý režim či dokonce přinesla některé nové problémy.

Založili jsme proto tento projekt, ve kterém se jako studenti snažíme vylepšit zkušenost dalších studentů. Náš cíl je vytvořit modernější, soukroumou a svobodnou verzi ŠOLu. Používáme aktuální vývojové praktiky a optimalizujeme UX pro mobily. Nikdy ale pravděpodobně nedosáhneme 100% podpory všech funkcí ŠOLu, například již kvůli jejich omezené API pro mobilní aplikace, kterou využíváme. Dále jsou limitující naše testovací možnosti, protože jako třetí strana můžeme testovat pouze věci, které se v ŠOLu objevují nám. `reŠOL` je v relativně rané fázi vývoje, momentálně nedokáže uspokojivě nahradit aplikaci Školy OnLine.

Vítáme každou pomoc, ať už s programováním či třeba grafikou.

*Upozornění: `reŠOL` není jakýmkoli způsobem ovlivněn či spojen se společností BAKALÁŘI software s.r.o.*

<h1>Jak to funguje?</h1>

U frontendu se řídíme pravidly:
- žádné špehování ([re-sol.tech](https://re-sol.tech) ale používá Cloudflare)
- svižné a non-bloated stránky

V JavaScriptu `reŠOL` dělá requesty na naší API napsanou v Rustu, která po *randomizaci user agentu a reffereru a skrze Tor proxy* odesílá requesty na Školu OnLine (každý request používá jinou Tor proxy). *Toto děláme kvůli soukromí uživatele, aby informace o jeho prohlížeči a IP zůstaly skryté.* Ta pak odesílá zpátky JSON soubor s daty, která parsujeme na stránku.

S tím se pojí pár problémů, jako pomalejší načítání a (hlavně) blokování requestů kvůli rate limitingu (ŠOL API má určitý počet requestů, které může uživatel udělat za určitý čas a jakmile tento limit překročí, tak je začne blokovat).
  
<h1>Funkce</h1>

Máme funkční přihlašování na (snad) všechny ŠOL servery. V headeru se na desktopu zobrazuje jméno a role (Jan Novák, student). Základní funkce rozvrhu fungují, jako zobrazování předmětů a učeben. Také lze rozkliknout jednotlivé předměty a zobrazit info o nich. V dashboardu zatím najdete funkční zprávy, hodnocení, domácí úkoly, akce a šablony pro ostatní boxy. Zprávy, známky a domácí úkoly lze rozklikávat pro zobrazení podrobností.

<h1>Použití</h1>

Provozujeme tyto instance, ale `reŠOL` lze také self-hostit:

- [re-sol.tech](https://re-sol.tech) (oficiální clear web stránka)
- [resol.i2p](http://resol.i2p/?i2paddresshelper=tg165pYFTjs8C5qD29UGde83nSZjzFSNzFtN2qMhxe62DXrmlgVOOzwLmoPb1QZ17zedJmPMVI3MW03aoyHF7rYNeuaWBU47PAuag9vVBnXvN50mY8xUjcxbTdqjIcXutg165pYFTjs8C5qD29UGde83nSZjzFSNzFtN2qMhxe62DXrmlgVOOzwLmoPb1QZ17zedJmPMVI3MW03aoyHF7rYNeuaWBU47PAuag9vVBnXvN50mY8xUjcxbTdqjIcXutg165pYFTjs8C5qD29UGde83nSZjzFSNzFtN2qMhxe62DXrmlgVOOzwLmoPb1QZ17zedJmPMVI3MW03aoyHF7rYNeuaWBU47PAuag9vVBnXvN50mY8xUjcxbTdqjIcXutg165pYFTjs8C5qD29UGde83nSZjzFSNzFtN2qMhxe62DXrmlgVOOzwLmoPb1QZ17zedJmPMVI3MW03aoyHF7mni-nS29H5WT1J9kaTWL1b0rXyHWdhgKifOxlnf9vvvBQAEAAcAAA==), [i2p base32 url](http://macnykyjbfgblgnjvdviihs4oo3cxyys2es3m6zx6bso4buqnuta.b32.i2p/) (i2p eepsite)
- [tor](http://resol47ut3awgsn27ulwmbugv6o4ksgybuqmjtggbqonfgf5pdg3clid.onion/) (tor stránka)

Nejvíce testování probíhá na Gecku (Firefox) a poté na Blinku (Chrome). Snažíme se také o WebKit (Safari), ale zaměřujeme se primárně na předešlé dva web enginy.

<h1>Vývoj</h1>

Více informací najdete na naší [wiki](/resol/resol/wiki/Home).

<h1>Licence</h1>

[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)](https://www.gnu.org/licenses/gpl-3.0)

`reŠOL` je svobodný software licencovaný pod [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html), což znamená, že máte
- Svobodu spustit program za jakýmkoliv účelem.
- Svobodu studovat, jak program pracuje a přizpůsobit ho svým potřebám.
- Svobodu redistribuovat kopie, abyste pomohli ostatním.
- Svobodu vylepšovat program a zveřejňovat zlepšení, aby z nich mohla mít prospěch celá komunita.

Tento program je distribuován s vírou, že bude užitečný, ale BEZ JAKÉKOLI ZÁRUKY; dokonce bez implicitní záruky PRODEJNÍ ZPŮSOBILOSTI nebo VHODNOSTI PRO KONKRÉTNÍ ÚČEL. Podívejte se na GNU General Public License pro více podrobností.

<h1>Poděkování</h1>

- https://bestofphp.com/repo/crpmax-skolaonline-app-api (stará API)

<h1>Ukázky</h1>

![rozvrh-light](assets/resol-rozvrh.png)
![rozvrh-dark](assets/resol-rozvrh-dark.png)
